package app.model;

public class PlaceSuggestion {

    private long id;
    private String name;
    private String address;
    private int type;
    private float latitude;
    private float longitude;
    private Long placeid;
    private long created;
    private Long completed;
    private long reportedby;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public Long getPlaceid() {
        return placeid;
    }

    public void setPlaceid(Long placeid) {
        this.placeid = placeid;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public Long getCompleted() {
        return completed;
    }

    public void setCompleted(Long completed) {
        this.completed = completed;
    }

    public long getReportedby() {
        return reportedby;
    }

    public void setReportedby(long reportedby) {
        this.reportedby = reportedby;
    }
}
