package app.model;

public class UserAccountHolder {

    private String username;
    private String password;
    private long accountcreated;
    private long passwordcreated;
    private String tokengenerated;
    private Long accounttype;
    private String resettoken;
    private Long deleted;
    private String profilepic;
    private long id;
    private long accountid;
    private String servicenumber;
    private String phonenumber;
    private String name;
    private int rank;
    private int regtcorps;
    private int type;
    private String address;
    private String emailaddress;
    private Long created;
    private int isregistered;


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Long getCreated() {
        return created;
    }
    public void setCreated(Long created) {
        this.created = created;
    }

    public long getPasswordcreated() {
        return passwordcreated;
    }

    public void setPasswordcreated(long passwordcreated) {
        this.passwordcreated = passwordcreated;
    }

    public String getTokengenerated() {
        return tokengenerated;
    }

    public void setTokengenerated(String tokengenerated) {
        this.tokengenerated = tokengenerated;
    }

    public Long getAccounttype() {
        return accounttype;
    }

    public void setAccounttype(Long accounttype) {
        this.accounttype = accounttype;
    }

    public String getResettoken() {
        return resettoken;
    }

    public void setResettoken(String resettoken) {
        this.resettoken = resettoken;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public long getAccountcreated() {
        return accountcreated;
    }

    public void setAccountcreated(long accountcreated) {
        this.accountcreated = accountcreated;
    }

    public long getAccountid() {
        return accountid;
    }

    public void setAccountid(long accountid) {
        this.accountid = accountid;
    }

    public String getServicenumber() {
        return servicenumber;
    }

    public void setServicenumber(String servicenumber) {
        this.servicenumber = servicenumber;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getRegtcorps() {
        return regtcorps;
    }

    public void setRegtcorps(int regtcorps) {
        this.regtcorps = regtcorps;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public int getIsregistered() {
        return isregistered;
    }

    public void setIsregistered(int isregistered) {
        this.isregistered = isregistered;
    }
}
