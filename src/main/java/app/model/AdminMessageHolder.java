package app.model;

public class AdminMessageHolder {

    private MessageDetails messageDetails;
    private VeteranDetail veteranDetail;


    public VeteranDetail getVeteranDetail() {
        return veteranDetail;
    }

    public void setVeteranDetail(VeteranDetail veteranDetail) {
        this.veteranDetail = veteranDetail;
    }

    public MessageDetails getMessageDetails() {
        return messageDetails;
    }

    public void setMessageDetails(MessageDetails messageDetails) {
        this.messageDetails = messageDetails;
    }
}
