package app.model;

public class AccountDetails {
    private Long id;
    private String username;
    private String password;
    private Long created;
    private Long passwordcreated;
    private String tokengenerated;
    private Long accounttype;
    private String resettoken;
    private Long deleted;
    private int forgotpasswordattempts;
    private Long lastpasswordresetattempt;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getPasswordcreated() {
        return passwordcreated;
    }

    public void setPasswordcreated(Long passwordcreated) {
        this.passwordcreated = passwordcreated;
    }

    public String getTokengenerated() {
        return tokengenerated;
    }

    public void setTokengenerated(String tokengenerated) {
        this.tokengenerated = tokengenerated;
    }

    public Long getAccounttype() {
        return accounttype;
    }

    public void setAccounttype(Long accounttype) {
        this.accounttype = accounttype;
    }

    public String getResettoken() {
        return resettoken;
    }

    public void setResettoken(String resettoken) {
        this.resettoken = resettoken;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getForgotpasswordattempts() {
        return forgotpasswordattempts;
    }

    public void setForgotpasswordattempts(int forgotpasswordattempts) {
        this.forgotpasswordattempts = forgotpasswordattempts;
    }

    public Long getLastpasswordresetattempt() {
        return lastpasswordresetattempt;
    }

    public void setLastpasswordresetattempt(Long lastpasswordresetattempts) {
        this.lastpasswordresetattempt = lastpasswordresetattempts;
    }
}