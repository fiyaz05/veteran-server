package app.service;

import app.constants.ApplicationConstants;
import app.model.AccountDetails;
import app.model.UserAccountHolder;
import app.model.VeteranDetail;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VeteranSignUpService {

    /**
     * Add userRoleMappingPopulating
     *
     * @param accountid  accountid
     * @param userrolid  userrolid
     * @param connection DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static Long userRoleMappingPopulating(long accountid, int userrolid, Connection connection) throws SQLException {

        if (accountid == 0L) {
            return null;
        }

        long time = System.currentTimeMillis();

        String sql = "insert into userrolemapping " +
                "(accountuserid, userroleid, created ) values (?,?,?)";

        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.insert(
                connection,
                sql,
                new ScalarHandler<Long>(),
                accountid,
                userrolid,
                time
        );

    }


    /**
     * Add veteranUserInsertRecord
     *
     * @param userAccountHolder userAccountHolder
     * @param connection        DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static Long insertVeteranUser(UserAccountHolder userAccountHolder, long accountid, Connection connection) throws SQLException {

        if (userAccountHolder == null) {
            return null;
        }

        long time = System.currentTimeMillis();
        QueryRunner queryRunner = new QueryRunner();
        String sql = "insert into veteran " +
                "(accountid, name, phonenumber, emailaddress, created ) values (?,?,?,?,?)";

        long result;
        result = queryRunner.insert(connection, sql, new ScalarHandler<Long>(), accountid,
                userAccountHolder.getName(),
                userAccountHolder.getPhonenumber(),
                userAccountHolder.getEmailaddress(),
                time);
        return result;
    }


    /**
     * Add query
     *
     * @param userAccountHolder userAccountHolder
     * @param connection        DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static Long accountDetailsInsert(UserAccountHolder userAccountHolder, Connection connection) throws SQLException, SQLIntegrityConstraintViolationException {

        int accounttype = ApplicationConstants.CLIENT_TYPE;
        if (userAccountHolder == null) {
            return null;
        }

        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedpassword = encoder.encode(userAccountHolder.getPassword());
        long time = System.currentTimeMillis();

        String sql = "insert into account " +
                "( username, password, created, passwordcreated, accounttype ) values (?,?,?,?,?)";

        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.insert(
                connection,
                sql,
                new ScalarHandler<Long>(),
                userAccountHolder.getUsername(),
                encodedpassword,
                time,
                time,
                accounttype);

    }

    /**
     * update veteran details
     *
     * @param veteranDetail
     * @param accountId
     * @return
     * @throws SQLException
     */
    public static Long updateVeteran(VeteranDetail veteranDetail, Long accountId, Connection connection) throws SQLException {

        if (veteranDetail == null) {
            return null;
        }

        long time = System.currentTimeMillis();

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update veteran set name = ? , " +
                "   emailaddress = ? , phonenumber = ? , " +
                "   profilepic = ?," +
                "   lastmodified = ?" +
                "   where accountid = ? ";

        long updatedCount = queryRunner.update(connection, updateStatement, veteranDetail.getName(),
                veteranDetail.getEmailaddress(), veteranDetail.getPhonenumber(),
                veteranDetail.getProfilepic(), time, accountId);

        return updatedCount;
    }

    /**
     * update veteran details
     *
     * @param veteranDetail
     * @return
     * @throws SQLException
     */
    public static boolean updateVeteranPicture(VeteranDetail veteranDetail, MultipartFile file, Connection connection) throws SQLException {

        if (veteranDetail == null) {
            return false;
        }
        AccountDetails accountDetails = AccountDetailsService.fetchAccount(veteranDetail.getAccountid(), connection);
        QueryRunner queryRunner = new QueryRunner();
        long time = System.currentTimeMillis();
        String updateStatement = "update veteran set profilepic = ?, lastmodified = ?" + "   where id = ? ";

        if (file != null) {
            veteranDetail.setProfilepic(uploadProfilePicture(accountDetails.getUsername(), file));
            Logger.getLogger("exception").log(Level.WARNING, "Profile image: " + veteranDetail.getProfilepic());
        }
        long updatedCount = queryRunner.update(connection, updateStatement, veteranDetail.getProfilepic(), time, veteranDetail.getId());
        return updatedCount > 0L;

    }

    public static String uploadProfilePicture(String name, MultipartFile file) {

        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();

                // Creating the directory to store file
                String rootPath = ApplicationConstants.VETERAN_PROFILE_DIR_PATH;

                File dir = new File(rootPath);
                if (!dir.exists())
                    dir.mkdirs();
                // Create the file on server
                File serverFile = new File(dir.getAbsolutePath() + File.separator + name + ".jpg");
                if (serverFile.exists()) {
                    serverFile.delete();
                }
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();

                return serverFile.getAbsolutePath();
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }

        }
        return null;
    }

}