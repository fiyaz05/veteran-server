package app.service;

import app.model.Admin;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class AdminService {

    public static List<Admin> fetchAllAdmin(Connection connection) throws SQLException {

        QueryRunner queryRunner = new QueryRunner();
        return queryRunner.query(
                connection, "select * from admin",
                new BeanListHandler<>(Admin.class));
    }

    /**
     * Fetches user information
     * @param id
     * @param connection
     * @return
     * @throws SQLException
     */
    public static Admin fetchAdminByAccountId(long id, Connection connection) throws SQLException {

        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.query(
                connection,
                "select * from admin where accountid = ?",
                new BeanHandler<>(Admin.class),
                id);
    }
}
