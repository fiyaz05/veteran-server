package app.service;

import app.model.VeteranDetail;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;

public class VeteranService {

    /**
     * Fetches user information
     *
     * @param id
     * @param connection
     * @return
     * @throws SQLException
     */
    public static VeteranDetail fetchVeteranByAccountId(long id, Connection connection) throws SQLException {


        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.query(
                connection,
                "select * from veteran where accountid = ?",
                new BeanHandler<>(VeteranDetail.class),
                id);
    }

    /**
     * Fetches user information
     *
     * @param veteranid
     * @param connection
     * @return
     * @throws SQLException
     */
    public static VeteranDetail fetchVeteranByVeteranId(Long veteranid, Connection connection) throws SQLException {


        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.query(
                connection,
                "select * from veteran where id = ?",
                new BeanHandler<>(VeteranDetail.class),
                veteranid);
    }

    public static boolean updateProfilePath(String profilePath, Long veteranid, Connection connection) throws SQLException {

        if (veteranid == null) {
            return false;
        }
        QueryRunner queryRunner = new QueryRunner();
        long time = System.currentTimeMillis();
        String updateStatement = "update veteran set profilepic = ?, lastmodified = ? " +
                "   where id = ?";
        int updatedCount = queryRunner.update(connection, updateStatement, profilePath, time, veteranid);

        return (updatedCount > 0);

    }

    public static String getProfilePath(Long veteranid, Connection connection) throws SQLException {

        if (veteranid == null) {
            return null;
        }
        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.query(
                connection,
                "select profilepic from veteran where id = ?",
                new ScalarHandler<String>(),
                veteranid);
    }
}
