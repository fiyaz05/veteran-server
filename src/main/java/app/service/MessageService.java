package app.service;

import app.constants.MessageConstants;
import app.model.AdminMessageHolder;
import app.model.MessageDetails;
import app.model.VeteranDetail;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class MessageService {

    /**
     * Add VeteranMessageInsertion
     * @param message message
     * @param connection DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static Long VeteranMessageInsertion (MessageDetails message, Connection connection) throws SQLException {

        if (message == null) {
            return null;
        }

        long time = System.currentTimeMillis();

        String sql = "insert into message " +
                "(fromid, toid, messagesentby, broadcast, message, created ) values (?,?,?,?,?,?)";

        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.insert(
                connection,
                sql,
                new ScalarHandler<Long>(),
                message.getFromid(),
                message.getToid(),
                MessageConstants.MESSAGE_SENT_BY_VETERAN,
                (message.getBroadcast() == 0 )? MessageConstants.NORMAL_MESSAGE : MessageConstants.BROADCAST_MESSAGE,
                message.getMessage(),
                time
        );

    }

    /**
     * Add AdminMessageInsertion
     * @param message message
     * @param connection DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static Long AdminMessageInsertion (MessageDetails message, Connection connection) throws SQLException {

        if (message == null) {
            return null;
        }
        QueryRunner queryRunner = new QueryRunner();

        long time = System.currentTimeMillis();

        String sql = "insert into message " +
                "(fromid, toid, messagesentby, broadcast, message, created, markstatus ) values (?,?,?,?,?,?,?)";

        return queryRunner.insert(
                connection,
                sql,
                new ScalarHandler<Long>(),
                message.getFromid(),
                message.getToid(),
                MessageConstants.MESSAGE_SENT_BY_ADMIN,
                (message.getBroadcast() == 0 )? MessageConstants.NORMAL_MESSAGE : MessageConstants.BROADCAST_MESSAGE,
                message.getMessage(),
                time,
                message.getMarkstatus()
        );

    }

    /**
     * Add getVeteranMessageById
     *
     * @param connection DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static MessageDetails getMessageById(Long id, Connection connection) throws SQLException {

        if (id == null) {
            return null;
        }

        QueryRunner queryRunner = new QueryRunner();

        String sql = "" +
                "select " +
                "	* " +
                "from " +
                "	message " +
                "where " +
                " id = ? ";

        return queryRunner.query(
                connection,
                sql,
                new BeanHandler<>(MessageDetails.class),
                id);

    }


    /**
     * Add getVeteranMessageById
     *
     * @param connection DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static List<MessageDetails> getVeteranMessageById(Long fromid, Long latestupdate, Connection connection) throws SQLException {

        if (fromid == null) {
            return null;
        }

        updateMessageDelieverStatusForVeteran(fromid,connection);

        QueryRunner queryRunner = new QueryRunner();

        String sql = "" +
                "select " +
                "	* " +
                "from " +
                "	message " +
                "where " +
                "	(( fromid = ? and messagesentby = ? ) " +
                "or (toid = ? and messagesentby = ? ) " +
                "or broadcast = ?) and latestupdate >= ? ";

        return queryRunner.query(
                connection,
                sql,
                new BeanListHandler<>(MessageDetails.class),
                fromid,
                MessageConstants.MESSAGE_SENT_BY_VETERAN,
                fromid,
                MessageConstants.MESSAGE_SENT_BY_ADMIN,
                MessageConstants.BROADCAST_MESSAGE,
                latestupdate
        );

    }


    /**
     * Add getAdminMessageById
     *
     * @param connection DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static List<AdminMessageHolder> getAdminMessageById(Long limit, Connection connection) throws SQLException {

        if (limit == null) {
            return null;
        }

        List<AdminMessageHolder> adminMessageHolderList = new LinkedList<>();
        QueryRunner queryRunner = new QueryRunner();

        List<MessageDetails> messages = queryRunner.query(
                                        connection,
                                        " select " +
                                                " * " +
                                                "from " +
                                                "(select *, IF (message.messagesentby = 1, message.fromid, message.toid) AS veteranid " +
                                                "from " +
                                                "message " +
                                                "where " +
                                                "message.broadcast = 0  " +
                                                "order by " +
                                                "created desc) as sub " +
                                                "group by " +
                                                "veteranid " +
                                                "order by " +
                                                "created desc " +
                                                "Limit ?",

                                        new BeanListHandler<>(MessageDetails.class),
                                        limit
        );

        for (MessageDetails message : messages) {

            AdminMessageHolder adminMessageHolder = new AdminMessageHolder();
            Long veteranid;
            if (message.getMessagesentby() == MessageConstants.MESSAGE_SENT_BY_VETERAN) {
                updateMessageDelieverStatusForAdmin(message.getFromid(), connection);
                veteranid = message.getFromid();
            } else {
                updateMessageDelieverStatusForAdmin(message.getToid(), connection);
                veteranid = message.getToid();
            }
            VeteranDetail veteranDetail = VeteranService.fetchVeteranByVeteranId(veteranid, connection);
            adminMessageHolder.setMessageDetails(message);
            adminMessageHolder.setVeteranDetail(veteranDetail);
            adminMessageHolderList.add(adminMessageHolder);
        }
        return adminMessageHolderList;
    }

    /**
     * Add getAdminConverationByVetranid
     *
     * @param veteranid
     * @param connection DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static List<MessageDetails> getAdminConverationByVetranid(Long veteranid, Connection connection) throws SQLException {

        if (veteranid == null) {
            return null;
        }

        updateMessageSeenStatusForAdmin(veteranid, connection);
        QueryRunner queryRunner = new QueryRunner();

        String sql = "" +
                "select " +
                "	* " +
                "from " +
                "	message " +
                "where " +
                "	( fromid = ? and messagesentby = ? ) " +
                "or (toid = ? and messagesentby = ? ) ";

        return queryRunner.query(
                connection,
                sql,
                new BeanListHandler<>(MessageDetails.class),
                veteranid,
                MessageConstants.MESSAGE_SENT_BY_VETERAN,
                veteranid,
                MessageConstants.MESSAGE_SENT_BY_ADMIN
        );

    }

    /**
     * Add updateMarkStatusForAdmin
     * @param connection DB connection
     * @param messageId Long
     * @return Created entity ID
     * @throws SQLException
     */
    public static boolean updateMarkStatusForAdmin (Long messageId, long markstatus, Connection connection) throws SQLException {

        if ( messageId == null) {
            return false;
        }

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update message set markstatus = ? " +
                "   where id = ? ";
        int updatedCount = queryRunner.update(connection, updateStatement, markstatus, messageId);

        return (updatedCount > 0);

    }

    /**
     * Add updateMessageDelieverStatusForAdmin
     * @param connection DB connection
     * @param fromid fromid
     * @return Created entity ID
     * @throws SQLException
     */
    public static boolean updateMessageDelieverStatusForAdmin (Long fromid, Connection connection) throws SQLException {

        if ( fromid == null) {
            return false;
        }

        long time = System.currentTimeMillis();

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update message set delivered = ? " +
                "   where fromid = ? and messagesentby = ? and delivered is null";
        int updatedCount = queryRunner.update(connection, updateStatement, time, fromid, MessageConstants.MESSAGE_SENT_BY_VETERAN);

        return (updatedCount > 0);

    }

    /**
     * Add updateMessageDelieverStatusForVeteran
     * @param connection DB connection
     * @param veteranid
     * @return Created entity ID
     * @throws SQLException
     */
    public static boolean updateMessageDelieverStatusForVeteran (Long veteranid, Connection connection) throws SQLException {

        if ( veteranid == null) {
            return false;
        }

        long time = System.currentTimeMillis();

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update message set delivered = ? " +
                "   where toid = ? and messagesentby = ? and delivered is null";
        int updatedCount = queryRunner.update(connection, updateStatement, time, veteranid, MessageConstants.MESSAGE_SENT_BY_ADMIN);

        return (updatedCount > 0);

    }

    /**
     * Add updateMessageSeenStatusForAdmin
     * @param connection DB connection
     * @param Veteranid
     * @return Created entity ID
     * @throws SQLException
     */
    public static boolean updateMessageSeenStatusForAdmin (Long Veteranid, Connection connection) throws SQLException {

        if ( Veteranid == null) {
            return false;
        }

        long time = System.currentTimeMillis();

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update message set seen = ? " +
                "   where fromid = ? and seen is null and messagesentby = ?";
        int updatedCount = queryRunner.update(connection, updateStatement, time, Veteranid,MessageConstants.MESSAGE_SENT_BY_VETERAN );

        return (updatedCount > 0);

    }

    /**
     * Add updateMessageSeenStatusForVeteran
     * @param connection DB connection
     * @param Veteranid
     * @return Created entity ID
     * @throws SQLException
     */
    public static boolean updateMessageSeenStatusForVeteran (Long Veteranid, Connection connection) throws SQLException {

        if ( Veteranid == null) {
            return false;
        }

        long time = System.currentTimeMillis();

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update message set seen = ? " +
                "   where toid = ? and seen is null and messagesentby = ?";
        int updatedCount = queryRunner.update(connection, updateStatement, time, Veteranid,MessageConstants.MESSAGE_SENT_BY_ADMIN );

        return (updatedCount > 0);

    }


    /**
     * Add getAllBroadcastMessageById
     * @param connection DB connection
     * @return Created entity ID
     * @throws SQLException
     */
    public static List<MessageDetails> getAllBroadcastMessageById (Connection connection) throws SQLException {

        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.query(
                connection,
                "select * from message where broadcast = ? order by id",
                new BeanListHandler<>(MessageDetails.class),
                MessageConstants.BROADCAST_MESSAGE
        );
    }

}
