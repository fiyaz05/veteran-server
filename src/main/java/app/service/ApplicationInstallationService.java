package app.service;

import app.model.AppInstallation;
import app.utils.ResultSetHandlers;
import app.utils.XQueryRunner;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ApplicationInstallationService {

    /**
     * Creates or updates new Installation record
     * @param installation
     * @param connection
     * @return
     * @throws SQLException
     */
    public static long createOrUpdateInstallation(AppInstallation installation, Connection connection) throws SQLException {

        if (installation == null) {
            return -1L;
        }

        long time = System.currentTimeMillis();

        XQueryRunner queryRunner = new XQueryRunner();

        String existingSql = "" +
                "select " +
                "	id " +
                "from " +
                "	appinstallation " +
                "where " +
                "	pushmessagingid = ? " +
                "	and deleted is null";

        Integer existingInstallationId = queryRunner.query(connection, existingSql, ResultSetHandlers.getKeyHandler(), installation.getPushmessagingid());

        if (existingInstallationId != null && existingInstallationId > 0L) {

            String updateSql = "" +
                    "update " +
                    "	appinstallation " +
                    "set " +
                    "	identifier = ?, " +
                    "	appversion = ?," +
                    "	lastmodified = ? " +
                    "where " +
                    "	id = ?";

            queryRunner.update(
                    connection,
                    updateSql,
                    installation.getIdentifier(),
                    installation.getAppversion(),
                    System.currentTimeMillis(),
                    existingInstallationId
            );

            return existingInstallationId;
        }

        String sql = "" +
                "insert into " +
                "	appinstallation " +
                "(ownerid, clienttype,  devicename, clientplatform, appversion, identifier, pushmessagingid, created, pushmessagingidgenerated) " +
                "	values (?,?,?,?,?,?,?,?,?) " +
                "on duplicate key " +
                "update " +
                "	pushmessagingid = ?, " +
                "	lastmodified = ?, " +
                "	pushmessagingidgenerated = ?";

        Integer installationId = queryRunner.insert(
                connection,
                sql,
                ResultSetHandlers.getKeyHandler(),
                installation.getOwnerid(),
                installation.getClienttype(),
                installation.getDeviceName(),
                installation.getClientplatform(),
                installation.getAppversion(),
                installation.getIdentifier(),
                installation.getPushmessagingid(),
                time,
                installation.getPushmessagingidgenerated(),
                installation.getPushmessagingid(),
                time,
                installation.getPushmessagingidgenerated()
        );

        return installationId != null && installationId > 0L ? installationId : 0L;
    }


    /**
     * Fetches user information
     *
     * @param accountId
     * @return
     * @throws SQLException
     */
    public static AppInstallation getAppByAccountId(Connection connection, long accountId) throws SQLException {

        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.query(
                connection, "select * from appinstallation where ownerid = ?",
                new BeanHandler<>(AppInstallation.class),
                accountId);
    }

    public static List<AppInstallation> getAppByOwnerId(Connection connection, long ownerId, int type ) throws SQLException {

        QueryRunner queryRunner = new QueryRunner();
        return queryRunner.query(
                connection, "select * from appinstallation where ownerid = ? and clienttype = ?",
                new BeanListHandler<>(AppInstallation.class), ownerId, type);
    }

    public static List<AppInstallation> getAllVeteranOwnerId(Connection connection, long clienttype) throws SQLException {

        QueryRunner queryRunner = new QueryRunner();
        return queryRunner.query(
                connection, "select * from appinstallation where clienttype = ?",
                new BeanListHandler<>(AppInstallation.class), clienttype);
    }

    /**
     * Updates push messaging id for an installation
     * @param pushMessagingId
     * @param newPushMessagingId
     * @param connection
     * @throws SQLException
     */
    public static void updatePushMessagingIdForInstallation(String pushMessagingId, String newPushMessagingId, Connection connection) throws SQLException {

        if (pushMessagingId == null || newPushMessagingId == null) {
            return;
        }

        XQueryRunner queryRunner = new XQueryRunner();

        String sql = "" +
                "update " +
                "	appinstallation " +
                "set " +
                "	pushmessagingid = ?, " +
                "	pushmessagingidgenerated = ?, " +
                "	lastmodified = ? " +
                "where " +
                "	pushmessagingid = ? " +
                "	and deleted is null";

        long time = System.currentTimeMillis();

        queryRunner.update(
                connection,
                sql,
                newPushMessagingId,
                time,
                time,
                pushMessagingId
        );
    }

    /**
     * Sets deleted for an installation
     * @param pushMessagingId
     * @param connection
     * @throws SQLException
     */
    public static void deleteInstallation(String pushMessagingId, Connection connection) throws SQLException {

        if (pushMessagingId == null) {
            return;
        }

        XQueryRunner queryRunner = new XQueryRunner();

        String sql = "" +
                "update " +
                "	appinstallation " +
                "set " +
                "	deleted = ? " +
                "where " +
                "	pushmessagingid = ?";

        long time = System.currentTimeMillis();

        queryRunner.update(
                connection,
                sql,
                time,
                pushMessagingId
        );
    }


}
