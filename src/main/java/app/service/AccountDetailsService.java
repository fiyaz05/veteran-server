package app.service;

import app.constants.ApplicationConstants;
import app.model.AccountDetails;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.Connection;
import java.sql.SQLException;

public class AccountDetailsService {

    /**
     * Fetches user information
     * @param id
     * @param connection
     * @return
     * @throws SQLException
     */
    public static AccountDetails fetchAccount(long id, Connection connection) throws SQLException {

        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.query(
                connection,
                "select * from account where id = ?",
                new BeanHandler<>(AccountDetails.class),
                id);
    }

    /**
     * Fetches user information
     * @param username
     * @param connection
     * @return
     * @throws SQLException
     */
    public static AccountDetails fetchAccountByUsername(String username, Connection connection) throws SQLException {


        QueryRunner queryRunner = new QueryRunner();

        return queryRunner.query(
                connection,
                "select * from account where username = ?",
                new BeanHandler<>(AccountDetails.class),
                username);
    }

    /**
     * update new password
     * @param id
     * @param connection
     * @return
     * @throws SQLException
     */
    public static boolean updateNewPassword (Long id,String newpassword, Connection connection) throws SQLException {

        if ( id == null) {
            return false;
        }

        long time = System.currentTimeMillis();

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update account set password = ? , passwordcreated = ? " +
                "   where id = ?";
        int updatedCount = queryRunner.update(connection, updateStatement, newpassword, time, id );

        return (updatedCount > 0);

    }

    /**
     * update Forgotpassword
     * @param id
     * @param connection
     * @return
     * @throws SQLException
     */
    public static boolean updateForgotPassword (Long id,String newpassword, Connection connection) throws SQLException {

        if ( id == null) {
            return false;
        }

        long time = System.currentTimeMillis();

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update account set password = ? ," +
                                                "   passwordcreated = ? ," +
                                                "   forgotpasswordattempts = ? ," +
                                                "   lastpasswordresetattempt = ? " +
                                                "   where id = ?";
        int updatedCount = queryRunner.update(connection, updateStatement, newpassword, time, ApplicationConstants.REFRESH_COUNT, time, id );

        return (updatedCount > 0);

    }

    /**
     * update Attempts
     * @param id
     * @param connection
     * @return
     * @throws SQLException
     */
    public static boolean updateNumberofAttempts (Long id, int attemps, Connection connection) throws SQLException {

        if ( id == null) {
            return false;
        }

        long time = System.currentTimeMillis();

        QueryRunner queryRunner = new QueryRunner();

        String updateStatement = "update account set forgotpasswordattempts = ? ," +
                "   lastpasswordresetattempt = ? " +
                "   where id = ?";
        int updatedCount = queryRunner.update(connection, updateStatement, attemps, time, id );

        return (updatedCount > 0);

    }


}
