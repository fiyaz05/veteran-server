package app.constants;

public class MessageConstants {

    public static final int MESSAGE_SENT_BY_ADMIN = 0;

    public static final int MESSAGE_SENT_BY_VETERAN = 1;

    public static final int NORMAL_MESSAGE = 0;

    public static final int BROADCAST_MESSAGE = 1;
}
