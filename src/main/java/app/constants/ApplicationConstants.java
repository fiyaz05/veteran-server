package app.constants;

public final class ApplicationConstants {

    //Client details
    public static final int PLATFORM_ANDROID = 1;

    //Client details
    public static final int CLIENT_TYPE = 1;

    //Admin details
    public static final int ADMIN_TYPE = 0;

    //Forgot Password details
    public static final int MAXIMUM_ATTEMPTS = 10;
    public static final int REFRESH_COUNT = 0;

    //Error messages
    public static final String ERROR_NOT_REGISTERED = "NotRegistered";

    public static final String VETERAN_PROFILE_DIR_PATH = "/data/veteran/profilepics/";

    public static final String ACTION = "action";
    public static final String ACTION_SYNC = "1";
    public static final String ACTION_CREATED_STATUS = "1";
    public static final String ACTION_SYNC_DELIVERED_STATUS = "2";
    public static final String ACTION_SYNC_SEEN_STATUS = "3";
    public static final String ACTION_UPDATED_STATUS = "4";

    public static final String COUNTRY_CODE = "IN";
}

