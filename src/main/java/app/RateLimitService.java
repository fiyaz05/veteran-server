package app;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RateLimitService {

    private static final int MAX_REQUESTS_IN_TIME_FRAME = 10;

    private LoadingCache<String, Integer> ratecache;
    private LoadingCache<String, Long> timecache;

    public RateLimitService() {
        super();
        ratecache = CacheBuilder.newBuilder().
                expireAfterWrite(1, TimeUnit.SECONDS).build(new CacheLoader<String, Integer>() {
            public Integer load(String key) {
                return 0;
            }
        });

        timecache = CacheBuilder.newBuilder().
                expireAfterWrite(1, TimeUnit.SECONDS).build(new CacheLoader<String, Long>() {
            public Long load(String key) {
                return 0L;
            }
        });
    }

    public boolean isRateLimitExceeded(String ip) {

        long timenow = System.currentTimeMillis();

        int rate;
        Long time;
        rate = ratecache.getUnchecked(ip);
        time = timecache.getUnchecked(ip);

        if (time == 0L) {
            rate = 0;

            timecache.put(ip, timenow);
            ratecache.put(ip, ++rate);

            return false;

        } else {
            ratecache.put(ip, ++rate);
            return rate > MAX_REQUESTS_IN_TIME_FRAME;
        }


    }


}
