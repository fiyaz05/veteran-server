package app;

import app.constants.ApplicationConstants;
import app.exceptions.ServerErrorException;
import app.model.AccountDetails;
import app.service.AccountDetailsService;
import app.service.LoginAttemptService;
import org.apache.commons.dbutils.DbUtils;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Configuration
@ComponentScan(basePackages = {"app"})
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;
    @Autowired
    private LoginAttemptService loginAttemptService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
            .authorizeRequests()
                .antMatchers(
                        "/profile/signup",
                        "/message/rate",
                        "/account/forgotpassword",
                        "/url/**")
                .permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login").successHandler(new AuthSuccessHandler())
                .failureHandler(new AuthFailureHandler())
                .usernameParameter("username").passwordParameter("password")
                .permitAll()
                .and()
            .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login")
                .permitAll()
                .and()
            .csrf()
                .disable()
            .headers()
                .cacheControl()
                .disable()
                .and()
            .exceptionHandling()
                .authenticationEntryPoint(new RESTAuthenticationEntryPoint());

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        String usersByUsernameQuery = "" +
                "select " +
                "	username, " +
                "	password, " +
                "	case when deleted is not null then 0 else 1 end as enabled " +
                "from " +
                "	account " +
                "where " +
                "	username = ?";

        String authoritiesByUsernameQuery = "" +
                "select " +
                "	account.username, " +
                "	userrole.role " +
                "from " +
                "	account " +
                "inner join " +
                "   userrolemapping on userrolemapping.accountuserid = account.id " +
                "inner join " +
                "	userrole on userrole.id = userrolemapping.userroleid " +
                "where " +
                "	account.username = ?";

        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(passwordEncoder())
                .usersByUsernameQuery(usersByUsernameQuery)
                .authoritiesByUsernameQuery(authoritiesByUsernameQuery);

    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    private String getClientIP(HttpServletRequest request) {
        final String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }

    private class AuthFailureHandler implements AuthenticationFailureHandler {

        @Autowired
        private MessageSource messages;

        @Override
        public void onAuthenticationFailure(HttpServletRequest httpServletRequest,
                                            HttpServletResponse httpServletResponse, AuthenticationException exception) throws IOException, ServletException {

            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            final String ip = getClientIP(httpServletRequest);
            loginAttemptService.loginFailed(ip);
            if (loginAttemptService.isBlocked(ip)) {
                httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        }

    }

    private class AuthSuccessHandler implements AuthenticationSuccessHandler {

        @Override
        public void onAuthenticationSuccess(HttpServletRequest request,
                                            HttpServletResponse response, Authentication authentication) throws IOException {
            handle(request, response, authentication);
            clearAuthenticationAttributes(request);
        }

        void handle(HttpServletRequest request,
                    HttpServletResponse response, Authentication authentication) throws IOException {

            Connection connection = null;
            try {

                connection = dataSource.getConnection();
                final String ip = getClientIP(request);
                if (loginAttemptService.isBlocked(ip)) {
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                } else {
                    AccountDetails user = AccountDetailsService.fetchAccountByUsername(authentication.getName(), connection);
                    if (user != null) {
                        request.getSession().setAttribute("user", user);
                    }

                    response.setStatus(HttpServletResponse.SC_OK);
                    if (user.getAccounttype() == ApplicationConstants.ADMIN_TYPE) {
                        response.getWriter().print(ApplicationConstants.ADMIN_TYPE);
                    } else {
                        response.getWriter().print(ApplicationConstants.CLIENT_TYPE);
                    }
                }

            } catch (SQLException ignored) {} finally {
                try {
                    connection.setAutoCommit(true);
                    DbUtils.close(connection);
                } catch (SQLException e) {
                    Logger.getLogger("finally").log(Level.WARNING, "Error: " + e);
                    throw new ServerErrorException(e);
                }
            }
        }

        void clearAuthenticationAttributes(HttpServletRequest request) {
            HttpSession session = request.getSession(false);
            if (session == null) {
                return;
            }
            session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        }
    }

    private class RESTAuthenticationEntryPoint implements AuthenticationEntryPoint {

        @Override
        public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
                throws IOException, ServletException {

            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
