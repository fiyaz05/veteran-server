package app.controller;

import app.constants.ApplicationConstants;
import app.exceptions.ServerErrorException;
import app.model.AppInstallation;
import app.service.ApplicationInstallationService;
import com.google.android.gcm.server.*;
import org.apache.commons.dbutils.DbUtils;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class GcmService {

    private static final String SENDER_KEY = "AIzaSyDTbFpBB6t6IqooFFFpVj1h90ucmjNzz8Q";
    private static final String PROJECT_NUMBER = "44811018121";
    private static final int RETRIES = 5;
    private static final int MULTICAST_SIZE = 1000;

    private final Executor threadPool = Executors.newFixedThreadPool(5);
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    public void sendToDevices(List<AppInstallation> installations, Message message) {

        if (installations == null || installations.size() == 0) {
            return;
        }

        int total = installations.size();
        List<String> partialDevices = new ArrayList<>(total);
        int counter = 0;

        for (AppInstallation installation : installations) {
            if (installation.getClientplatform() == ApplicationConstants.PLATFORM_ANDROID) {
                counter++;
                partialDevices.add(installation.getPushmessagingid());

                int partialSize = partialDevices.size();
                if (partialSize == MULTICAST_SIZE || counter == total) {
                    asyncSend(partialDevices, message);
                    partialDevices.clear();
                }

            }
        }

    }

    private void asyncSend(List<String> partialDevices, final Message message) {

        final List<String> devices = new ArrayList<>(partialDevices);

        threadPool.execute(new Runnable() {

            public void run() {
                MulticastResult multicastResult;
                try {
                    Sender sender = new Sender(SENDER_KEY);
                    multicastResult = sender.send(message, devices, RETRIES);
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
                List<Result> results = multicastResult.getResults();
                // analyze the results
                for (int i = 0; i < devices.size(); i++) {

                    String regId = devices.get(i);
                    Result result = results.get(i);
                    String messageId = result.getMessageId();

                    if (messageId != null) {
                        String canonicalRegId = result.getCanonicalRegistrationId();
                        if (canonicalRegId != null) {
                            updateGCMRegistrationIdWrapper(regId, canonicalRegId);
                        }

                    } else {

                        String error = result.getErrorCodeName();
                        if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
                            // application has been removed from device -
                            // unregister it
                            deleteRegistrationIdWrapper(regId);
                        } else if (error.equals(Constants.ERROR_MISMATCH_SENDER_ID)) {
                            Logger.getLogger("GCM").log(Level.WARNING, "Mismatch sender id");
                        }
                    }
                }
            }
        });
    }

    private void updateGCMRegistrationIdWrapper(String pushMessagingId,
                                                String newPushMessagingId) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            ApplicationInstallationService.updatePushMessagingIdForInstallation(pushMessagingId, newPushMessagingId, connection);

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    private void deleteRegistrationIdWrapper(String pushMessagingId) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            ApplicationInstallationService.deleteInstallation(pushMessagingId, connection);

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    public void sendSeenStatus(List<AppInstallation> appInstallations) {

        Message.Builder syncMessageBuilder = new Message.Builder()
                .addData(ApplicationConstants.ACTION, ApplicationConstants.ACTION_SYNC_SEEN_STATUS)
                .collapseKey(ApplicationConstants.ACTION_SYNC_SEEN_STATUS);

        sendToDevices(appInstallations, syncMessageBuilder.build());
    }

    public void sendDeliveredStatus(List<AppInstallation> appInstallations) {
        Message.Builder syncMessageBuilder = new Message.Builder()
                .addData(ApplicationConstants.ACTION, ApplicationConstants.ACTION_SYNC_DELIVERED_STATUS)
                .collapseKey(ApplicationConstants.ACTION_SYNC_DELIVERED_STATUS);

        sendToDevices(appInstallations, syncMessageBuilder.build());
    }

    public void sendCreatedStatus(List<AppInstallation> appInstallations) {
        Message.Builder syncMessageBuilder = new Message.Builder()
                .addData(ApplicationConstants.ACTION, ApplicationConstants.ACTION_CREATED_STATUS)
                .collapseKey(ApplicationConstants.ACTION_CREATED_STATUS);

        sendToDevices(appInstallations, syncMessageBuilder.build());
    }

    public void sendUpdatedStatus(List<AppInstallation> appInstallations) {
        Message.Builder syncMessageBuilder = new Message.Builder()
                .addData(ApplicationConstants.ACTION, ApplicationConstants.ACTION_UPDATED_STATUS)
                .collapseKey(ApplicationConstants.ACTION_UPDATED_STATUS);

        sendToDevices(appInstallations, syncMessageBuilder.build());
    }

}
