package app.controller;

import app.constants.ApplicationConstants;
import app.exceptions.ServerErrorException;
import app.model.AccountDetails;
import app.model.VeteranDetail;
import app.service.AccountDetailsService;
import app.service.VeteranService;
import org.apache.commons.dbutils.DbUtils;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@RestController
@RequestMapping("/account")
public class UserAccountController {
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    @RequestMapping(method = RequestMethod.POST, value = "/changepassword")
    public void passwordChanging(
            @RequestParam String oldpassword,
            @RequestParam String newpassword,
            HttpSession session,
            HttpServletResponse response) {

        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetails = (AccountDetails) session.getAttribute("user");
            if (accountDetails != null) {

                PasswordEncoder encoder = new BCryptPasswordEncoder();
                String encodednewpassword = encoder.encode(newpassword);

                if (encoder.matches(oldpassword, accountDetails.getPassword())) {
                    AccountDetailsService.updateNewPassword(accountDetails.getId(), encodednewpassword, connection);
                    response.setStatus(HttpServletResponse.SC_OK);
                    session.invalidate();
                } else {
                    response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
                }
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }

    }

    @RequestMapping(method = RequestMethod.POST, value = "/forgotpassword")
    public void forgotPassword(
            @RequestParam String username,
            @RequestParam String phonenumber,
            @RequestParam String newpassword,
            HttpServletResponse response) {

        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetails = AccountDetailsService.fetchAccountByUsername(username, connection);

            if (accountDetails != null) {
                VeteranDetail veteranDetail = VeteranService.fetchVeteranByAccountId(accountDetails.getId(), connection);

                if (accountDetails.getForgotpasswordattempts() <= ApplicationConstants.MAXIMUM_ATTEMPTS) {

                    if (username.equals(accountDetails.getUsername()) && phonenumber.equals(veteranDetail.getPhonenumber())) {
                        PasswordEncoder encoder = new BCryptPasswordEncoder();
                        String encodednewpassword = encoder.encode(newpassword);
                        AccountDetailsService.updateForgotPassword(accountDetails.getId(), encodednewpassword, connection);
                        response.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        int attempts = accountDetails.getForgotpasswordattempts();
                        AccountDetailsService.updateNumberofAttempts(accountDetails.getId(), attempts + 1, connection);
                        response.getWriter().print(ApplicationConstants.MAXIMUM_ATTEMPTS - accountDetails.getForgotpasswordattempts());
                        response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
                    }

                } else {
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                }

            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }

    }
}
