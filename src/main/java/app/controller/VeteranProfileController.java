package app.controller;

import app.constants.ApplicationConstants;
import app.exceptions.InvalidInputException;
import app.exceptions.ServerErrorException;
import app.model.AccountDetails;
import app.model.VeteranDetail;
import app.service.VeteranService;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import org.apache.commons.dbutils.DbUtils;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;

@RestController
@RequestMapping("/profile")
public class VeteranProfileController {


    private static final Logger logger = LoggerFactory.getLogger(VeteranProfileController.class);
    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @RequestMapping(method = RequestMethod.GET, value = "/veteran")
    public VeteranDetail getVeteranByAccount(HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            AccountDetails account = (AccountDetails) session.getAttribute("user");
            return VeteranService.fetchVeteranByAccountId(account.getId(), connection);
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(value = "/picture", method = RequestMethod.POST)
    @ResponseBody
    public void uploadProfilePicture(@RequestBody MultipartFile file, HttpSession session) {

        File serverFile = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetails = (AccountDetails) session.getAttribute("user");

                VeteranDetail veteranDetail = VeteranService.fetchVeteranByAccountId(accountDetails.getId(), connection);
                String name = accountDetails.getUsername() + "-" + System.currentTimeMillis();
                if (!file.isEmpty()) {
                    try {
                        byte[] bytes = file.getBytes();

                        // Creating the directory to store file
                        String rootPath = ApplicationConstants.VETERAN_PROFILE_DIR_PATH;
                        File dir = new File(rootPath);
                        if (!dir.exists())
                            dir.mkdirs();
                        // Create the file on server

                        serverFile = new File(dir.getAbsolutePath() + File.separator + name + ".jpg");
                        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                        stream.write(bytes);
                        stream.close();
                        MagicMatch match = Magic.getMagicMatch(serverFile, false, true);
                        if (match.getMimeType().equals(MimeTypeUtils.IMAGE_JPEG_VALUE)) {
                            VeteranService.updateProfilePath(serverFile.getName(), veteranDetail.getId(), connection);
                        } else {
                            throw new InvalidInputException("Invalid image format");
                        }
                    } catch (MagicMatchNotFoundException e) {
                        if (serverFile.exists()) {
                            serverFile.delete();
                        }
                        throw new InvalidInputException("Invalid image format");
                    } catch (InvalidInputException e) {
                        throw new InvalidInputException("Invalid image format");
                    } catch (Exception e) {
                        throw new InvalidInputException("Invalid image format");
                    }
                } else {
                }
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }


    @RequestMapping(value = "/picture", method = RequestMethod.GET)
    public
    @ResponseBody
    void downloadFiles(@RequestParam Long veteranid,
            HttpServletResponse response, HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            AccountDetails account = (AccountDetails) session.getAttribute("user");
            Long accounttype = account.getAccounttype();
            if (accounttype == ApplicationConstants.ADMIN_TYPE) {
                downloadImage(veteranid, response, connection);
            } else if (accounttype == ApplicationConstants.CLIENT_TYPE) {
                VeteranDetail veteranDetail = VeteranService.fetchVeteranByAccountId(account.getId(), connection);
                if (veteranDetail.getId() == veteranid) {
                    downloadImage(veteranid, response, connection);
                } else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    private void downloadImage(Long veteranid, HttpServletResponse response, Connection connection) throws SQLException, IOException {
        String path = VeteranService.getProfilePath(veteranid, connection);
        if (path != null) {
            String rootPath = ApplicationConstants.VETERAN_PROFILE_DIR_PATH;
            File dir = new File(rootPath + path);
            File file = new File(dir.getAbsolutePath());
            InputStream is = new FileInputStream(file);

            // MIME type of the file
            response.setContentType("application/octet-stream");
            // Response header
            response.setHeader("Content-Disposition", "attachment; filename=\""
                    + file.getName() + "\"");
            response.setHeader("Cache-Control", "no-transform, public, max-age=3600");
            // Read from the file and write into the response
            OutputStream os = response.getOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) != -1) {
                os.write(buffer, 0, len);
            }
            os.flush();
            os.close();
            is.close();
            response.setStatus(HttpServletResponse.SC_OK);
        }
    }
}