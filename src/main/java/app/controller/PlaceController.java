package app.controller;

import app.exceptions.ServerErrorException;
import app.model.AccountDetails;
import app.model.Place;
import app.model.PlaceSearchResult;
import app.model.PlaceSuggestion;
import app.utils.XQueryRunner;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/place")
public class PlaceController {

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @RequestMapping(method = RequestMethod.GET, value = "/nearby")
    public List<Place> getPlaces(
            @RequestParam(name = "latitude") double latitude,
            @RequestParam(name = "longitude") double longitude,
            @RequestParam(name = "distance", required = false) Double distance
    ) {

        Connection connection = null;

        try {

            connection = dataSource.getConnection();

            XQueryRunner queryRunner = new XQueryRunner();

            if (distance == null) {
                distance = 25d;
            }

            String sql = "" +
                    "SELECT " +
                    "   place.*, " +
                    "   ( 3959 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin( radians( latitude ) ) ) ) AS distance " +
                    "FROM " +
                    "   place " +
                    "HAVING " +
                    "   distance < ? " +
                    "ORDER BY " +
                    "   distance ";

            return queryRunner.query(
                    connection,
                    sql,
                    new BeanListHandler<>(Place.class),
                    latitude, longitude, latitude, distance
            );

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    private final static int ESM_CELL = 1;
    private final static int ECHS_POLYCLINIC = 2;
    private final static int CANTEEN = 3;
    private final static int SAINIK_RESTHOUSE = 4;
    private final static int CANTEEN_OUTLET = 5;

    @RequestMapping(method = RequestMethod.GET, value = "/search/{query}")
    public List<PlaceSearchResult> getPlaces(
            @PathVariable("query") String query
    ) {

        Connection connection = null;

        try {

            connection = dataSource.getConnection();

            XQueryRunner queryRunner = new XQueryRunner();

            if (query.length() < 3) {
                return null;
            }

            query = query.toLowerCase();

            String sql = String.format("" +
                    "SELECT " +
                    "   place.* " +
                    "FROM " +
                    "   place " +
                    "WHERE " +
                    "   lower(name) like '%%%s%%' " +
                    "ORDER BY " +
                    "   name ", query);

            List<Place> places = queryRunner.query(
                    connection,
                    sql,
                    new BeanListHandler<>(Place.class)
            );

            if (places == null || places.size() == 0) {
                return null;
            }

            LinkedList<PlaceSearchResult> results = new LinkedList<>();
            HashMap<String, LinkedList<Place>> outletsMap = new HashMap<>();
            for (final Place place : places) {

                if (place.getType() == CANTEEN_OUTLET) {
                    List<Place> otherOutlets = outletsMap.get(place.getName());
                    if (otherOutlets == null) {
                        outletsMap.put(place.getName(), new LinkedList<Place>());
                        otherOutlets = outletsMap.get(place.getName());

                        PlaceSearchResult result = new PlaceSearchResult();
                        result.setName(place.getName());
                        result.setType(CANTEEN_OUTLET);
                        result.setPlaces(otherOutlets);

                        results.add(result);
                    }

                    otherOutlets.add(place);
                    continue;
                }

                PlaceSearchResult result = new PlaceSearchResult();
                result.setName(place.getName());
                result.setType(place.getType());
                result.setPlaces(new ArrayList<Place>() {{
                    add(place);
                }});

                results.add(result);

            }

            return results;

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/suggestion")
    public void suggestPlace(@RequestBody PlaceSuggestion placeSuggestion, HttpSession session) {

        AccountDetails account = (AccountDetails) session.getAttribute("user");

        Connection connection = null;
        try {

            connection = dataSource.getConnection();

            placeSuggestion.setReportedby(account.getId());

            XQueryRunner queryRunner = new XQueryRunner();
            queryRunner.insert(
                    connection,
                    "insert into placesuggestion (name, address, type, latitude, longitude, placeid, created, completed, reportedby) values (?,?,?,?,?,?,?,?,?)",
                    new ScalarHandler<Long>(),
                    placeSuggestion.getName(),
                    placeSuggestion.getAddress(),
                    placeSuggestion.getType(),
                    placeSuggestion.getLatitude(),
                    placeSuggestion.getLongitude(),
                    placeSuggestion.getPlaceid(),
                    placeSuggestion.getCreated(),
                    placeSuggestion.getCompleted(),
                    placeSuggestion.getReportedby()
            );

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }

    }
}
