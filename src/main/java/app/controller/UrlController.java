package app.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/url")
public class UrlController {

    @RequestMapping(method = RequestMethod.GET, value = "/pension")
    public void pensionUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://cgda.nic.in/Suvigya/index.html");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/coursemate")
    public void coursemateUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://indianarmyveterans.gov.in/v_search.php?lang=1");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/veteransportal")
    public void veteranportalUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://indianarmyveterans.gov.in");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/indianarmy")
    public void indianarmyUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://indianarmy.nic.in ");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/joinindianarmy")
    public void joinindianarmyUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://joinindianarmy.nic.in");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/pcdapension")
    public void pcdapensionUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://pcdapension.nic.in");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/dgrindia")
    public void dgrindiaUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://dgrindia.com");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ksbgov")
    public void ksbgovUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://ksb.gov.in");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/awesindia")
    public void awesindiaUrl(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://awesindia.com ");
    }
}
