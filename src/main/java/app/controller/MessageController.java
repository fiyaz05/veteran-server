package app.controller;

import app.constants.ApplicationConstants;
import app.constants.MessageConstants;
import app.exceptions.InvalidRequestException;
import app.exceptions.ServerErrorException;
import app.model.*;
import app.service.AdminService;
import app.service.ApplicationInstallationService;
import app.service.MessageService;
import app.utils.DBUtils;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;
    @Autowired
    private GcmService gcmService;


    @RequestMapping(method = RequestMethod.POST, value = "/veteran")
    @Secured(value = "ROLE_USER")
    public MessageDetails messageFromVeteran(@RequestBody MessageDetails messageDetails, HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetailusername = (AccountDetails) session.getAttribute("user");
            if (accountDetailusername != null) {
                Long veteranid = DBUtils.getIdFromAccountId(accountDetailusername.getId(), connection);
                messageDetails.setFromid(veteranid);

                if (messageDetails.getToid() != null) {
                    throw new InvalidRequestException("Invalid request");
                }

                if (messageDetails.getBroadcast() == MessageConstants.BROADCAST_MESSAGE) {
                    throw new  InvalidRequestException("Cannot send broadcast message. Insufficient permissions");
                }

                Long messageid = MessageService.VeteranMessageInsertion(messageDetails, connection);
                if (messageid > 0L) {
                    com.google.android.gcm.server.Message.Builder messageBuilder = new com.google.android.gcm.server.Message.Builder()
                            .addData("message", "Message received.");
                    List<Admin> adminList = AdminService.fetchAllAdmin(connection);
                    List<AppInstallation> appInstallations = null;
                    for (Admin admin : adminList) {
                        appInstallations = ApplicationInstallationService.getAppByOwnerId(connection, admin.getId(), ApplicationConstants.ADMIN_TYPE);
                    }
                    gcmService.sendToDevices(appInstallations, messageBuilder.build());
                    MessageDetails insertedMessage = MessageService.getMessageById(messageid, connection);
                    return insertedMessage;
                }
            }
            return null;
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin")
    @Secured(value = "ROLE_ADMIN")
    public boolean messageFromAdmin(@RequestBody MessageDetails messageDetails, HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetailusername = (AccountDetails) session.getAttribute("user");
            if (accountDetailusername != null) {
                Long adminid = DBUtils.getIdFromAccountId(accountDetailusername.getId(), connection);
                messageDetails.setFromid(adminid);
                Long messageid = MessageService.AdminMessageInsertion(messageDetails, connection);
                if (messageid > 0L) {
                    if (messageDetails.getToid() != null) {
                        List<AppInstallation> appInstallations = ApplicationInstallationService.getAppByOwnerId(connection, messageDetails.getToid(), ApplicationConstants.CLIENT_TYPE);
                        if (appInstallations.size() > 0) {
                            gcmService.sendCreatedStatus(appInstallations);
                        }
                    } else {
                        List<AppInstallation> appInstallations = ApplicationInstallationService.getAllVeteranOwnerId(connection, ApplicationConstants.CLIENT_TYPE);
                        if (appInstallations.size() > 0) {
                            gcmService.sendCreatedStatus(appInstallations);
                        }
                    }
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/veteran")
    @Secured(value = "ROLE_USER")
    public List<MessageDetails> getVeteranMessages(@RequestParam Long latestupdate, HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetailusername = (AccountDetails) session.getAttribute("user");
            Long veteranid = DBUtils.getIdFromAccountId(accountDetailusername.getId(), connection);
            return MessageService.getVeteranMessageById(veteranid, latestupdate, connection);
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin")
    @Secured(value = "ROLE_ADMIN")
    public List<AdminMessageHolder> getAdminMessages(@RequestParam Long limit, HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            List<AdminMessageHolder> messageHolders = MessageService.getAdminMessageById(limit, connection);
            List<AppInstallation> appInstallations = null;
            if (messageHolders.size() > 0) {
                for (AdminMessageHolder messageHolder : messageHolders) {
                    if (messageHolder.getVeteranDetail() != null) {
                        appInstallations = ApplicationInstallationService.getAppByOwnerId(connection, messageHolder.getVeteranDetail().getId(), ApplicationConstants.CLIENT_TYPE);
                    }
                }
                gcmService.sendDeliveredStatus(appInstallations);
            }
            return MessageService.getAdminMessageById(limit, connection);
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/conversation")
    @Secured(value = "ROLE_ADMIN")
    public List<MessageDetails> getMessagesByVeteranId(@RequestParam Long veteranid, HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            List<MessageDetails> messageDetails = MessageService.getAdminConverationByVetranid(veteranid, connection);
            if (messageDetails.size() > 0) {
                List<AppInstallation> appInstallations = ApplicationInstallationService.getAppByOwnerId(connection, veteranid, ApplicationConstants.CLIENT_TYPE);
                gcmService.sendSeenStatus(appInstallations);
            }
            return messageDetails;
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/markstatus")
    @Secured(value = "ROLE_ADMIN")
    public boolean updateMarkstatus(@RequestParam Long messageid, long markstatus, HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            return MessageService.updateMarkStatusForAdmin(messageid, markstatus, connection);

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/veteran/seen")
    @Secured(value = "ROLE_USER")
    public boolean updateSeenStatusForVeteran(HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetailusername = (AccountDetails) session.getAttribute("user");
            Long id = DBUtils.getIdFromAccountId(accountDetailusername.getId(), connection);

            return MessageService.updateMessageSeenStatusForVeteran(id, connection);
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/broadcast")
    public List<MessageDetails> getAllbroadcastMessages() {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();

            return MessageService.getAllBroadcastMessageById(connection);
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/rate")
    public String rate() {
        return RandomStringUtils.randomAlphanumeric(10);
    }
}