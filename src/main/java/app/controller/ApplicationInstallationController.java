package app.controller;

import app.constants.ApplicationConstants;
import app.exceptions.ServerErrorException;
import app.model.AccountDetails;
import app.model.Admin;
import app.model.AppInstallation;
import app.model.VeteranDetail;
import app.service.AdminService;
import app.service.ApplicationInstallationService;
import app.service.VeteranService;
import org.apache.commons.dbutils.DbUtils;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("/installation")
public class ApplicationInstallationController {

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @RequestMapping(value = "/registeradmin", method = RequestMethod.POST)
    public boolean registerAdmin(@RequestBody AppInstallation appInstallation, HttpSession session) {
        AccountDetails accountDetails = (AccountDetails) session.getAttribute("user");
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            Admin adminDetail = AdminService.fetchAdminByAccountId(accountDetails.getId(), connection);
            appInstallation.setOwnerid(adminDetail.getId());
            appInstallation.setClienttype(ApplicationConstants.ADMIN_TYPE);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.setAutoCommit(true);
                DbUtils.close(connection);
            } catch (SQLException e) {
                Logger.getLogger("finally").log(Level.WARNING, "Error: " + e);
                throw new ServerErrorException(e);
            }
        }

        return register(appInstallation);
    }

    @RequestMapping(value = "/registeruser", method = RequestMethod.POST)
    public boolean registerUser(@RequestBody AppInstallation appInstallation, HttpSession session) {
        AccountDetails accountDetails = (AccountDetails) session.getAttribute("user");
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            VeteranDetail veteranDetail = VeteranService.fetchVeteranByAccountId(accountDetails.getId(), connection);
            appInstallation.setOwnerid(veteranDetail.getId());
            appInstallation.setClienttype(ApplicationConstants.CLIENT_TYPE);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.setAutoCommit(true);
                DbUtils.close(connection);
            } catch (SQLException e) {
                Logger.getLogger("finally").log(Level.WARNING, "Error: " + e);
                throw new ServerErrorException(e);
            }
        }

        return register(appInstallation);
    }

    private boolean register(AppInstallation appInstallation) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();

            long id = ApplicationInstallationService.createOrUpdateInstallation(appInstallation, connection);
            return id > 0L;
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }

        }
    }

}
