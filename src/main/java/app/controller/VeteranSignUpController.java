package app.controller;

import app.constants.ApplicationConstants;
import app.constants.VeteranSignUpConstant;
import app.exceptions.InvalidRequestException;
import app.exceptions.ServerErrorException;
import app.model.AccountDetails;
import app.model.AppInstallation;
import app.model.UserAccountHolder;
import app.model.VeteranDetail;
import app.service.ApplicationInstallationService;
import app.service.VeteranService;
import app.service.VeteranSignUpService;
import app.utils.DBUtils;
import app.utils.GenericUtils;
import app.utils.PasswordUtils;
import com.google.gson.GsonBuilder;
import org.apache.commons.dbutils.DbUtils;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.passay.RuleResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("/profile")
public class VeteranSignUpController {
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;
    @Autowired
    GcmService gcmService;
    private String validationError;


    @RequestMapping(method = RequestMethod.POST, value = "/signup")
    public String createUserRecord(@RequestBody UserAccountHolder userAccountHolder) {
        return registerVeteranUser(userAccountHolder);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/uploadphoto")
    public boolean uploadPhoto(@RequestBody MultipartFile multipartFile, HttpSession session) {
        Logger.getLogger("exception").log(Level.WARNING, "File size: " + multipartFile.getSize());
        return updateVeteranPhoto(multipartFile, session);

    }

    private boolean updateVeteranPhoto(MultipartFile multipartFile, HttpSession session) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetails = (AccountDetails) session.getAttribute("user");
            Long veteranid = DBUtils.getIdFromAccountId(accountDetails.getId(), connection);
            VeteranDetail veteranDetail = VeteranService.fetchVeteranByAccountId(accountDetails.getId(),
                    connection);
            return VeteranSignUpService.updateVeteranPicture(veteranDetail, multipartFile, connection);
        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    private String registerVeteranUser(UserAccountHolder userAccountHolder) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            RuleResult result = PasswordUtils.get().validPassword(userAccountHolder.getPassword());
            if (!result.isValid()) {
                throw new InvalidRequestException(PasswordUtils.get().getMessage(result));
            }

            Long accountid = VeteranSignUpService.accountDetailsInsert(userAccountHolder, connection);
            if (accountid > 0L) {
                Long userroleresultid = VeteranSignUpService.userRoleMappingPopulating(accountid, VeteranSignUpConstant.VETERAN_ACCOUNT, connection);
                if (userroleresultid > 0L) {
                    Long resultid = null;
                    resultid = VeteranSignUpService.insertVeteranUser(userAccountHolder, accountid, connection);
                    if (resultid > 0L) {
                        VeteranDetail veteranDetail = VeteranService.fetchVeteranByAccountId(accountid, connection);
                        return new GsonBuilder().create().toJson(veteranDetail);
                    }
                }
            }
            return null;
        } catch (SQLException e) {
            if (e.getMessage().indexOf("unique constraint") != 0) {
                Logger.getLogger("exception").log(Level.WARNING, "Error: " + e);
                throw new ServerErrorException("Something went wrong");
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            throw new ServerErrorException(e.getMessage());
        } finally {
            try {
                connection.setAutoCommit(true);
                DbUtils.close(connection);
            } catch (SQLException e) {
                Logger.getLogger("finally").log(Level.WARNING, "Error: " + e);
                throw new ServerErrorException(e);
            }
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updateveterandetail")
    public VeteranDetail updateVeteranDetails(@RequestBody VeteranDetail veteranDetail, HttpSession session) {
        return updateVeteran(veteranDetail, session);
    }


    public VeteranDetail updateVeteran(VeteranDetail veteranDetail, HttpSession session) {
        Connection connection = null;

        VeteranDetail updatedVeteranDetail;
        try {
            connection = dataSource.getConnection();
            AccountDetails accountDetails = (AccountDetails) session.getAttribute("user");

            if (veteranDetail != null) {
                Long resultid = null;
                resultid = VeteranSignUpService.updateVeteran(veteranDetail, accountDetails.getId(), connection);
                if (resultid > 0L) {
                    List<AppInstallation> appInstallations = ApplicationInstallationService.getAppByOwnerId(connection, veteranDetail.getId(), ApplicationConstants.CLIENT_TYPE);
                    veteranDetail = VeteranService.fetchVeteranByAccountId(accountDetails.getId(), connection);
                    gcmService.sendUpdatedStatus(appInstallations);
                }
                return veteranDetail;
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new ServerErrorException(e);
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                throw new ServerErrorException(e);
            }
        }
    }

    private boolean validation(UserAccountHolder userAccountHolder) {
        if (userAccountHolder.getName().isEmpty()) {
            validationError = "Required field: name";
            return false;
        } else if (!userAccountHolder.getEmailaddress().isEmpty()) {
            if (!GenericUtils.isValidEmailAddress(userAccountHolder.getEmailaddress())) {
                validationError = "Please enter valid email";
                return false;
            }
        } else if (userAccountHolder.getPhonenumber().isEmpty()) {
            validationError = "Required field: phone number";
            return false;
        } else if (userAccountHolder.getUsername().isEmpty()) {
            validationError = "Required field: username";
            return false;
        } else if (userAccountHolder.getPassword().isEmpty()) {
            validationError = "Required field: password";
            return false;
        } else if (!GenericUtils.isValidPhoneNumber(userAccountHolder.getPhonenumber())) {
            validationError = "Please enter valid phone number.";
            return false;
        }
        return true;
    }

    private boolean validation(VeteranDetail veteranDetail) {
        if (veteranDetail.getName().isEmpty()) {
            validationError = "Required field: name";
            return false;
        } else if (!veteranDetail.getEmailaddress().isEmpty()) {
            if (!GenericUtils.isValidEmailAddress(veteranDetail.getEmailaddress())) {
                validationError = "Please enter valid email";
                return false;
            }
        } else if (veteranDetail.getPhonenumber().isEmpty()) {
            validationError = "Required field: phone number";
            return false;
        } else if (!GenericUtils.isValidPhoneNumber(veteranDetail.getPhonenumber())) {
            validationError = "Please enter valid phone number.";
            return false;
        }
        return true;
    }
}
