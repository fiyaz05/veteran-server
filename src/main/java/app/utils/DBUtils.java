package app.utils;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by mohamed on 30/3/16.
 */
public class DBUtils {

    public static Long getIdFromAccountId(Long accountid, Connection connection) throws SQLException {
        if( accountid == null){
            return null;
        }

        QueryRunner queryRunner = new QueryRunner();

        int accounttype = queryRunner.query(
                connection,
                "select accounttype from account where id = ?",
                new ScalarHandler<Integer>(),
                accountid
        );

        if( accounttype == 1){
            int veteranid = queryRunner.query(
                    connection,
                    "select id from veteran where accountid = ?",
                    new ScalarHandler<Integer>(),
                    accountid
            );

            return Long.valueOf(veteranid);
        }else {
            int adminid = queryRunner.query(
                    connection,
                    "select id from admin where accountid = ?",
                    new ScalarHandler<Integer>(),
                    accountid
            );
            return Long.valueOf(adminid);
        }

    }

}
