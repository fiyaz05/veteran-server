package app.utils;

import org.passay.*;

import java.util.Arrays;
import java.util.List;

public class PasswordUtils {

    private static PasswordUtils instance;

    public static PasswordUtils get() {

        if (instance == null) {
            instance = new PasswordUtils();
            instance.init();
        }

        return instance;
    }

    private void init() {

        List<Rule> rules = Arrays.asList(
                new LengthRule(8, 30),
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1),
                new CharacterRule(EnglishCharacterData.Special, 1),
                new WhitespaceRule()
        );

//        try {
//
//            InputStream inputStream = getClass().getResourceAsStream("classpath:passwordblacklist/blacklist.txt");
//            Reader reader = new InputStreamReader(inputStream);
//
//            DictionaryRule rule = new DictionaryRule(
//                    new WordListDictionary(WordLists.createFromReader(
//                            // Reader around the word list file
//                            new Reader[] { reader },
//                            // True for case sensitivity, false otherwise
//                            false,
//                            // Dictionaries must be sorted
//                            new ArraysSort())));
//
//            rules.add(rule);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        validator = new PasswordValidator(rules);

    }

    private PasswordValidator validator;

    public RuleResult validPassword(String password) {
        return validator.validate(new PasswordData(password));
    }

    public String getMessage(RuleResult result) {

        List<String> messages = validator.getMessages(result);
        if (messages != null && messages.size() > 0) {
            return messages.get(0);
        }

        return null;

    }
}
