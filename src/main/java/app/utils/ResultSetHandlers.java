package app.utils;

import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetHandlers {

	private final static ResultSetHandler<Integer> sKeyHandler = new ResultSetHandler<Integer>() {

		@Override
		public Integer handle(ResultSet resultSet) throws SQLException {
			
			if (!resultSet.next()) {
				return null;
			}
			
			Integer result = resultSet.getInt(1);
			
			return result;
		}
		
	};
	
	public static ResultSetHandler<Integer> getKeyHandler() {
		return sKeyHandler;
	}
}
