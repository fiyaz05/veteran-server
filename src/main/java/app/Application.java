package app;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@Import({WebSecurityConfig.class})
public class Application implements EmbeddedServletContainerCustomizer {

    private static final boolean PRODUCTION = true;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean(name = "dataSource")
    public DataSource dataSource() {

        String DB_URL = PRODUCTION
                ? "jdbc:mysql://localhost:3306/outreach?useUnicode=yes&characterEncoding=UTF-8"
                : "jdbc:mysql://52.36.235.95:3306/outreach?useUnicode=yes&characterEncoding=UTF-8";

        DataSource dataSource = new DataSource();
        dataSource.setName("outreachdb");
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl(DB_URL);
        dataSource.setUsername("outreach");
        dataSource.setPassword("pHNetworks123!");
        dataSource.setMaxActive(100);
        dataSource.setMaxIdle(8);
        dataSource.setInitialSize(10);
        dataSource.setValidationQuery("select 1");
        dataSource.setValidationInterval(60000);
        dataSource.setTestOnBorrow(true);
        dataSource.setTestOnConnect(true);
        dataSource.setTestOnReturn(true);
        dataSource.setTestWhileIdle(true);

        return dataSource;
    }

    @Override
    public void customize(ConfigurableEmbeddedServletContainer configurableEmbeddedServletContainer) {

//        ((TomcatEmbeddedServletContainerFactory) configurableEmbeddedServletContainer).addConnectorCustomizers(new TomcatConnectorCustomizer() {
//
//            @Override
//            public void customize(final Connector connector) {
//                connector.setSecure(true);
//            }
//        });

    }
}
