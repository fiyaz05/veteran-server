package app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid request")
public class InvalidRequestException extends RuntimeException {

    Exception mE;
    String mMessage;

    public InvalidRequestException(Exception e) {
        e.printStackTrace();
        mE = e;
    }

    public InvalidRequestException(String message) {
        mMessage = message;
    }

    @Override
    public String getMessage() {

        if (mE != null) {
            return mE.getMessage();
        }

        if (!StringUtils.isEmpty(mMessage)) {
            return mMessage;
        }

        return null;

    }

}
