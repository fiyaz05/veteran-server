package app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Seenivasan on 2/5/16.
 */
@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid Parameter")
public class InvalidInputException extends RuntimeException {

    Exception mE;
    String mMessage;

    public InvalidInputException(Exception e) {
        e.printStackTrace();
        mE = e;
    }

    public InvalidInputException(String message) {
        Logger.getLogger("Message").log(Level.WARNING, "Error: " + message);
        mMessage = message;
    }

    @Override
    public String getMessage() {

        if (mE != null) {
            return mE.getMessage();
        }

        if (mMessage != null) {
            return mMessage;
        }

        return null;

    }
}
