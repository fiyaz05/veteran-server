package app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Something went wrong")
public class ServerErrorException extends RuntimeException {

    Exception mE;
    String mMessage;

    public ServerErrorException(Exception e) {
        e.printStackTrace();
        mE = e;
    }

    public ServerErrorException(String message) {
        mMessage = message;
    }

    @Override
    public String getMessage() {

        if (mE != null) {
            return mE.getMessage();
        }

        if (mMessage != null) {
            return mMessage;
        }

        return null;

    }
}