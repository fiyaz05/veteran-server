package app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Invalid credentials")
public class ForbiddenException extends RuntimeException{
}
